# M150 - Vertiefungsarbeit
Im Rahmen des Moduls "150 - E-Business-Applikationen anpassen" haben wir uns einer selbstständigen Vertiefungsarbeit gewidmet. Dafür haben wir uns für folgende Themen entscheiden:
* Eine Dokumentatation über Design, Layout und SEO bei Webseiten
* Ein Style Guide für die Firma online marketing by Wiebke Wesselmann
    
## Dokumentation
* [Design bei Webseiten](/Dokumentation/Design.md)
* [Layouts bei Webseiten](/Dokumentation/Layout.md)
* [SEO bei Webseiten](/Dokumentation/SEO.md)

Unter Berücksichtigung dieses Wissens haben wir anschliessend den Style Guide erstellt.

## Style Guide
* [Style Guide](/Style Guide/OMWW_StyleGuide.pdf)

Anhand des Style Guides haben wir Designvorschläge für die Desktopansicht und auch für die mobile Ansicht entwickelt.

### Desktop
* [Produktseite](/Style Guide/OMWW_Layout_D_Produktseite.pdf)
* [Warenkorb](/Style Guide/OMWW_Layout_D_Warenkorb.pdf)

### Mobile
* [Produktseite](/Style Guide/OMWW_Layout_M_Produktseite.pdf)
* [Warenkorb](/Style Guide/OMWW_Layout_M_Warenkorb.pdf)
* [Dropdown-Menü](/Style Guide/OMWW_Layout_M_dropdown.pdf)