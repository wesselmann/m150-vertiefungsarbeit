# Design
In diesem Abschnitt dreht sich alles um das richtige Design für Webseiten und auf was man bei der Erstellung achten sollte.

## Bilder
Wenn ein Kunde in einem Onlineshop ein Produkt kaufen will, genügt die Beschreibung allein nicht aus. Egal, wie detailliert die Produktbeschreibung ist, der Käufer will immer ein Produkt visuell betrachten. Dabei muss das Bild in hoher Qualität zur Verfügung gestellt werden, denn schlechte Bilder sind genau gleich, wie die Produktbeschreibung ohne Bild.   Der Kunde baut mit Bildern ein Vertrauen zum Produkt auf, wodurch er sich sicher fühlt und den Kauf eher tätigen wird. 

Nicht nur für Onlineshops, sondern auch für Dienstleistungen und andere Arten von Webseiten sind Grafiken und Bilder ein Muss. Aus psychologischer Sicht baut der Kunde eine Verbindung zur Webseite auf. Beispielsweise sind verschiedene Personen oder lachende Gesichter auf der Webseite einer Telekommunikationsfirma zu sehen. Die Firma versucht ihre Kunden zu erreichen, indem sie das Gefühl vermittelt, die Kaufenden seien sehr zufrieden mit ihrer Leistung.  Auch wenn der Kunde die Bilder nur im Hintergrund betrachtet, erscheint die Webseite im Gesamten als hell und freundlich. Deshalb sind hochqualitative Grafiken und Bilder nahezu eine Pflicht auf einer Webseite. Aber die Menge an Bildern auf einer Seite sollte niemals übertrieben werden. Eine überladene Seite wirkt wiederum weniger professionell und unüberschaubar. 

_Sunrise-Webseite_

![Webseite von Sunrise](/Dokumentation/images/Sunrise_Webseite.png "Sunrise-Webseite")


## Farben
Farben sind einer der mächtigsten Werkzeuge im Design. Das hat auch seinen Grund. In Farben sind laut psychologischen Studien verschiedene Emotionen versteckt, die geweckt werden können. Folgende Grafik stellt die Emotionen verschiedener Farben dar:

_Farben & die damit verbundenen Emotionen_

![Einige Fragen mit ihren verbundenen Emotionen](/Dokumentation/images/Farben.png "Farben & die damit verbundenen Emotionen")

Ein Warenkorb-Button in einem Onlineshop findet man meist in roter Farbe, weil diese den Eindruck der Aufregung vermittelt. Vegane Produkte sind meistens in grüner Tonlage zu finden, weil diese natürlich und frisch wirken. Die Bestimmung der Farbe hat enorm grosse Auswirkung auf verschiedene Arten von Zielgruppen. Nur schon die Logofarbe bestimmt, ob ein Kunde sich diese Seite ansieht oder nicht. Viele grosse Firmen nutzen die Psychologie der Farben in ihren Logos, Webseiten sowie Produkten, um ihre Konsumenten stark zu beeinflussen.

Ein Besucher der Seite merkt es niemals direkt, was die Farbe in ihn auslöst. Die Emotionen dahinter werden automatisch in ihm erweckt, sodass der Besucher genau das tut, was sich die Firma vorgestellt hat. Nämlich den Kauf des Produktes oder der Dienstleistungen. Die Farbe sollte deshalb sehr bedacht und zielgerichtet auf die Konsumentengruppe gewählt werden.


## Corporate Design
Die Marke und das Branding sind wichtige Bestandteile der heutigen Menschheit. Heutzutage mögen es viele Menschen, wenn sie ein Markenprodukt kaufen oder einen bekannten Dienstleistungsanbieter benutzen. Niemand will von einer Webseite ein Produkt kaufen, die aussieht, als würde es die Kreditkartendetails stehlen. Das wird meist durch Corporate Design verhindert. Dieses prägt die Webseite so aus, dass es ein visuelles Erscheinungsbild wiedergibt, welches in allen Angelegenheiten der Firma wieder auffindbar ist. Beispielsweise hat die Bank UBS ein rot-weiss-schwarzes Erscheinungsbild . Sie setzt es auf allen Webseiten sowie Brandprodukten konstant um. 

_UBS Webseite und Apps_

![Webseite von UBS](/Dokumentation/images/UBS-Webseite.png "UBS-Webseite") ![Übersicht mit eineigen Apps von UBS](/Dokumentation/images/UBS-App.png "UBS-Apps")

Dabei spielt nicht nur die Farbe eine Rolle, sondern auch Schrift, Bild- und Grafikverwendungen. All diese Bestandteile sorgen dafür, dass die Marke UBS immer wiederzuerkennen ist. Der Wiedererkennungswert ist ein wichtiger Faktor, um die Kunden vermehrt zum Unternehmen zu locken. Je deutlicher und einzigartiger das Design ist, desto mehr Kunden bauen Vertrauen zur Firma auf, wodurch die Anzahl der Interaktion  steigt. 


## Zielgruppen
Jede Webseite ist schlussendlich für die Zielgruppe erstellt worden. Der Enduser sollte möglichst oft und lange auf der Seite verbleiben. Aber wer sind die Enduser das Unternehmen? Diese Frage sollte sich jeder Webdesigner grundsätzlich stellen, denn das gesamte Design hängt von dieser Frage ab. Wenn der Enduser ein junges Mädchen ist, das nach Onlinespielen sucht, sollte die Webseite möglichst einfach und unkompliziert sein. Dies trifft aber auch auf einen älteren Herrn Mitte 70 zu, der gerne einen Seniorenclub besuchen möchte. Je nach Art der Zielgruppe muss das Design angepasst werden.

Um dies zu erreichen, sollte sich der Designer in die Position des Users versetzen. Dadurch kann das Design leicht bestimmt werden. 

* Auf welche Weise ergibt es am meisten Sinn, die Elemente zu organisieren? 
* Was sollte mein Enduser als Erstes betrachten oder erreichen können? 
* Welche Wirkung sollte die Webseite auf meine Zielgruppe haben? 

Fragen, wie diese helfen das Design zu verwirklichen. Dadurch wird auch sichergestellt, dass die Erwartungen der Zielgruppe erfüllt werden.


## Quellen
* <a href="https://99designs.de/blog/web-digitales-design/e-commerce-webdesign/" target="_blank">99designs.de - eCommerce Webdesign</a>
* <a href="https://99designs.de/logo-design/psychology-of-color" target="_blank">99designs.de - Psychologie von Farben</a>
* <a href="https://www.tbzwiki.ch/index.php?title=Design_%26_Layout" target="_blank">tbzwiki.ch - Design & Layout</a>
* <a href="https://marketing.ch/lexikon/corporate-design/" target="_blank">marketing.ch - Corporate Design</a>
* <a href="https://www.sunrise.ch/de/privatkunden/mobil-abos/up-mobile-c.html" target="_blank">sunrise.ch - Mobile Abos</a>
* <a href="https://play.google.com/store/apps/details?id=com.ubs.prod.myhubmobile&hl=en_NZ&gl=US" target="_blank">play.google.com - UBS Apps</a>
* <a href="https://www.ubs.com/ch/de/private/accounts-and-cards.html" target="_blank">ubs.com - Konten & Karten</a>