# SEO und die Suchmaschine Google
Dieser Abschnitt behandelt die Wichtigkeit von SEO und erklärt, was genau die Suchmaschinenoptimierung ist und welche Massnahmen diese beinhaltet. Da Google im europäischen Raum die wichtigste Suchmaschine ist, beziehen sich alle Zahlen und Daten im folgenden Abschnitt auf diese Suchmaschine.


## Was ist SEO?
Im World Wide Web wird ständig gesucht – sei es nach bestimmten Webseiten, Informationen oder nach Orten. Für all das werden Suchmaschinen benötigt. Aktuell verarbeitet Google 3.9 Milliarden Suchanfragen pro Tag.    Mittlerweile generieren unzählige Unternehmen über ihre Webseite Umsätze. Damit dies funktioniert und möglichst viele Einnahmen erzielt werden, muss diese Seite jedoch erst einmal gefunden werden. Das wird durch die Suchmaschinenoptimierung erreicht.

Bei der Suchmaschinenoptimierung, auch SEO (Search Engine Optimization) genannt, geht es einfach gesagt darum, eine Webseite so zu optimieren, dass Suchmaschinen diese als sehr gut einstufen und mehr anzeigen. Es fallen also alle Massnahmen, die dazu beitragen, dass die Webseite besser wird, in diesen Bereich. Unterscheiden wird dabei noch zwischen On-Page- und Off-Page-Massnahmen. Als Bestandteil des Onlinemarketings ist es eine langfristige Massnahme, die mehrere Monate bis sogar Jahre dauern kann.

Mit SEO will man so viele organische Klicks wie möglich generieren. Organisch ist ein Klick dann, wenn dieser nicht mittels Werbeschaltung generiert wurde, sondern dadurch, dass der Anzeigetext bei der Suchmaschine ansprechend ist oder der Link weit oben auf der Google-Ergebnisseite erscheint.

## Wieso SEO?
68% aller Internetbesuche starten mit Google. Besonders Unternehmen, die mit ihrer Webseite Umsätze generieren, sind auf Massnahmen zur Suchmaschinenoptimierung angewiesen, da es essenziell ist, dass Kunden die Webseite besuchen. Es muss also auf die Webseite aufmerksam gemacht werden.

Ein kleines Beispiel: 
Max ist auf der Suche nach einem neuen Rucksack. Deswegen besucht er die Suchmaschine Google und tippt in das Suchfeld «Rucksack» ein. Sofort erscheinen zehn verschiedene Links, auf die Max klicken kann. Jeder Link führt zu einem anderen Angebot von einem anderen Webshop. Über 60% der Google-Nutzer klicken bei der Suche auf den ersten Link. Auch Max gehört dazu und kauft seinen neuen Rucksack dort.

_Diagramm der prozentualen Klickwahrscheinlichkeit für die Position auf der Google Suchergebnisseite (Daten von sistrix.de)_

![Diagramm der prozentualen Klickwahrscheinlichkeit für die Position auf der Google Suchergebnisseite (Daten von sistrix.de)](/Dokumentation/images/Klickwahrscheinlichkeit.png "Diagramm der prozentualen Klickwahrscheinlichkeit für die Position auf der Google Suchergebnisseite (Daten von sistrix.de)")

Wenn man sich einmal die Zahlen anschaut, fällt ganz klar auf, wer nicht unter den ersten zehn Suchergebnissen (die erste Seite) bei Google landet, hat verloren. 99,1% aller organischen Klicks (also die Ergebnisse ohne Werbeschaltung) finden auf der ersten Google-Seite statt. Auf das allererste Suchergebnis klicken knapp 60% der Internetnutzer, auf das zweite nur noch knapp 16% und auf das dritte Ergebnis sogar nur noch 8%. Nur 0.17% aller Google-Nutzer besuchen überhaupt die zweite Seite und damit das elfte Ergebnis.

Gerade wenn ein Unternehmen Geld über die Homepage oder den Webshop generiert, ist eine hohe Platzierung auf der Google Suchergebnisseite nötig. Dazu kommt, dass in den meisten Fällen die SEO-Massnahmen 55% aller Klicks ausmachen. Die Werbeanzeigen bei Google nur rund 5%.


## Was sind die Ziele von SEO?
Hauptziel der Suchmaschinenoptimierung ist es natürlich, bei Google auf Platz 1 zu landen, wenn ein User nach einem relevanten Thema des Unternehmens sucht. Einhergehend ist es das Ziel, die Besuche der Webseite zu erhöhen. Es reicht schliesslich nicht, wenn die Webseite auf der obersten Position ist, wenn die Besucher ausbleiben. Hier spielt die Kurzbeschreibung auf Google eine wichtige Rolle. Durch die Massnahmen von SEO sollte der Traffic (alle Besuche und Interaktionen mit einer Webseite) auf 40 – 60% gesteigert werden.

_Beispiel eines Google Anzeigetextes_

![Beispiel eines Google Anzeigetextes](/Dokumentation/images/SERP.png "Beispiel eines Google Anzeigetextes")


## Google Ranking
Google bewertet alle Webseiten, die es finden und in ihren Index aufnehmen sollen. Dafür hat es ein Algorithmus mit über 200 verschiedenen Faktoren und Kriterien entwickelt . Anhand dieser Kriterien wird jede einzelne Seite analysiert und anschliessend in das Google Ranking eingetragen. Dieses Ranking entscheidet später darüber, an welcher Position die Webseite in den Suchergebnissen aufgelistet wird.  

Es ist ein weitverbreiteter Irrtum, dass das Unternehmen, welches am meisten dafür zahlt, die beste Position erhält. Qualität ist für eine gute Platzierung viel ausschlaggebender. 


## Umsetzung
Bis jetzt wurde nur behandelt, was genau SEO ist und warum es so wichtig ist. Doch wie genau sehen die Massnahmen der Suchmaschinenoptimierung eigentlich aus? Im Grossen und Ganzen braucht es für eine effektive Optimierung ein Zusammenspiel aus On-Page-Faktoren (Webseite) und Off-Page-Faktoren (externe Einflüsse).

### On-Page
Kurz und knapp gehören zu den On-Page-Massnahmen der Inhalt und das Design einer Webseite. Beim Aufbau und Design sollten sieben Kriterien beachtet und umgesetzt werden.

__Keywords:__ Keywords, oder Schlüsselwörter auf Deutsch, sind Wörter, die ein Nutzer in das Suchfeld der Suchmaschine eingibt. Jedes Unternehmen hat bestimmte Keywords, unter denen es gefunden werden möchte. Deswegen ist eine Massnahme der Suchmaschinenoptimierung, die Inhalte auf der Webseite mit relevanten Keywords zu erweitern und auch Synonyme einzubinden. Wenn ein User nun beispielsweise Rucksack in die Suchleiste eingibt, sucht Google im Hintergrund alle indexierten Webseiten durch und überprüft, ob dieses Keyword auf einer Seite erwähnt wird. Aber Achtung – Google bestraft Keyword-Spamming. Es ist also nicht zu empfehlen, die entsprechenden Schlüsselwörter übertrieben oft zu erwähnen .  

_On-Page-Faktoren auf der TBZ-Homepage_

![On-Page-Faktoren auf der TBZ-Homepage](/Dokumentation/images/OnPage_Massnahmen.png "On-Page-Faktoren auf der TBZ-Homepage")

__Meta-Titel:__ Der Meta-Titel ist der Webseitentitel im Register-Tab. Er ist Teil der HTML-Struktur der Webseite und das wichtigste Element. Im Meta-Titel wird der Inhalt der Seite mittels relevanten Keywords wiedergegeben.  Im Idealfall ist dieser Titel 50 Zeichen lang, nicht aber länger als 70.  Wenn der Text zu lang ist, kann nicht mehr der ganze Inhalt im Tab angezeigt werden.

__URL:__ Bei der URL kommt es vor allem auf einen logischen Aufbau an. Besonders kurze und gut lesbare URLs sind die Devise. Denn so können sich Besucher diese auch ganz einfach merken oder zwischen den Unterseiten navigieren. Zusätzlich spielt auch die Sprache der URL eine Rolle. Wenn möglich, sollte die Sprache der URL zur Sprache des Seiteninhalts passen.

__Navigation:__ Bei der Navigation gibt es die sieben-plus/minus-zwei-Regel, an die sich alle SEO-Umsetzer halten sollten. Diese Regel besagt, dass die Navigation maximal fünf bis neun Elemente enthalten soll. Denn mehr kann das menschliche Gehirn gar nicht wahrnehmen. Die Menüpunkte in der Navigation sind zudem relevante Suchbegriffe, also die Keywords des Unternehmens. 

__Alt-Tags:__ Alt-Tags kommen bei der Bildbeschreibung zum Zug und sind ausserordentlich wichtig. Schliesslich möchten auch Menschen mit einer Sehschwäche sich im Internet zurechtfinden. Das Alt-Tag bei Bildern ermöglicht Screenreadern die Informationen, die ein Bild vermitteln sollen, vorzulesen. Der Googlebot, der Webseiten sucht und bewertet, ist ebenfalls auf das Alt-Tag angewiesen, da dieser Bilder ebenfalls nicht «lesen» kann.

__Headline H1:__ Als H1 wird der Haupttitel der Webseite beschrieben. Pro Seite gibt es genau einen. Dieser beschreibt das Thema der jeweiligen Seite und soll optisch herausstechen. Die Headline ist in der Regel zwischen 20 und 70 Zeichen lang und beantwortet im besten Fall direkt die Frage des Benutzers. Das Hauptkeyword der jeweiligen Webseite muss im H1-Titel erwähnt werden. Wenn auf der Seite mehr als ein Titel vonnöten ist, werden Abstufungen ab H2 verwendet.

__Leadtext:__ Die ersten 200 – 350 Zeichen auf einer Webseite haben auf das Ranking bei Google einen hohen Einfluss. Denn wenn von Webseitenbetreiber nichts anderes definiert wurde, dient der Leadtext als Anzeigetext bei den Google Suchergebnis (siehe Abbildung 2). Des Weiteren entscheidet sich hier ebenfalls, ob die angewählte Seite erstens ansprechend geschrieben und zweitens relevant für den Leser ist, sodass dieser weiterliest.

__Content:__ Der eigentliche Inhalt der Webseite wird auch Content genannt. Hier finden sich alle Texte, Bilder und sonstige Seiteninhalte wieder. Keywords und relevante Informationen müssen hier so verpackt werden, dass die Seite einerseits für den Googlebot ansprechend ist und andererseits natürlich auch für die Besucher. Deswegen ist eine einfache und verständliche Sprache mit kurzen Sätzen zu empfehlen.

### Off-Page
Bei den Off-Page-Massnahmen geht es um alle Faktoren, die ausserhalb der Webseite stattfinden, wie das Verhalten der User oder Backlinks (externe Links, die auf die Webseite verweisen). Backlinks sind Links von Dritt-Webseiten auf die eigene Webseite. Also Webseiten, die nicht zum Unternehmen gehören. Denn wenn Links auf eine Webseite führen, kann man in vielen Fällen davon ausgehen, dass die verlinkte Seite von guter Qualität und hilfreich ist. Das merkt der Googlebot und lässt es in das Webseitenranking einfliessen. Für die verlinkte Seite ist die Qualität des Verlinkenden von hoher Bedeutung, denn auch das spielt für Google eine Rolle. Backlinks sollten niemals eingekauft werden. Das führt zu einer Abstrafung oder sogar zum Ausschluss aus der Suchmaschine. Bei Backlinks zählt die Weisheit “Qualität über Quantität” besonders.

__Passiver Backlink-Aufbau:__ Passiv ist der Link-Aufbau, wenn Verlinkende von allein einen Backlink setzen. Also, wenn die Webseite von hoher Relevanz und Qualität für diese ist. Als Webseiteneigentümer nimmt man keine Massnahmen vor, ausser die Schaffung von einzigartigem und hochwertigem Content.

__Aktiver Backlink-Aufbau:__ Sobald der Backlink absichtlich (aber ohne diesen einzukaufen) gesetzt wird, ist die Rede vom aktiven Link-Aufbau. Das ist beispielsweise der Fall, wenn ein Gastartikel auf einem Blog veröffentlicht wird und der Autor oder dessen Unternehmen verlinkt werden. Links, die über die sozialen Medien verteilt werden, gehören ebenfalls in diese Kategorie.

Die Off-Page-Faktoren haben viel mit den weitergehenden Marketingmassnahmen zu tun. So zählen Brand Building (sich als gute Marke positionieren), Social-Media-Aktivitäten, öffentliche Firmenevents und auch das Influencer-Marketing  (bekannte Persönlichkeiten machen für das Unternehmen Werbung) dazu.

### Technisches SEO
Hochwertiger Inhalt reicht heutzutage nicht mehr aus, um ein gutes Google Ranking zu erzielen. Neben den klassischen On-Page- und Off-Page-Massnahmen muss auch das technische Grundgerüst stimmen. Hierzu gehören schnelle Ladezeiten der Webseiten, Bilder und Videos, eine sichere Webseite und ein validierter Code.

__Kanonische URL:__ Es kann vorkommen, dass von einer Webseite mehrere Versionen existieren. Die Originalversion wird mithilfe des Canonical-Tags für die Suchmaschinen gekennzeichnet, sodass nur diese für die Indexierung und Bewertung berücksichtigt werden. Das ist wichtig, da mehrfach vorkommender Inhalt (Duplicated Content) – sprich 1 zu 1 kopierte Seiten – von Google bestraft und schlecht bewertet werden.

__Robots.txt:__ Crawler (Googlebots, die Webseiten absuchen und in den Index aufsuchen) können gezielt auf Webseiten gelenkt beziehungsweise blockiert werden. Das Ganze wird in einer Textdatei, genannt robots.txt, vermerkt und im Domainverzeichnis abgelegt. 

__SSL-Zertifikat:__ Die Sicherheit der User im Internet sollte überall grossgeschrieben werden. Deswegen ist auch das ein wichtiger Punkt im Bereich des SEO. Mittels SSL-Zertifikats wird eine verschlüsselte Datenübertragung sichergestellt. Ob eine Seite dies umsetzt, ist anhand der URL zu erkennen. Sichere Webseiten beginnen mit HTTPS.

__Ladegeschwindigkeit:__ Eine Webseite darf maximal 3 Sekunden laden, um ein hier ein gutes Ranking zu erhalten. Um dies zu erreichen, können Massnahmen wie Bilder komprimieren oder interne Weiterleitungen minimieren, getroffen werden.

__Mobile SEO:__ Es ist bekannt, dass mobile Endgeräte eine immer grösser werdende Rolle in unserem Alltag spielen. Bereits jetzt werden 75% aller Webzugriffe von einem mobilen Endgerät (Smartphone oder Tablet) getätigt. Seit dem 1. Juli 2019 herrscht bei Google die Mobile-First-Indexierung. Google nutzt also vorrangig die mobile Version der Webseite für die Indexierung und Bewertung. 


## Negative SEO
Nicht alle Massnahmen, die für die Suchmaschinenoptimierung getroffen werden, helfen bei einer guten Bewertung. Ganz im Gegenteil sogar – es gibt auch negative SEO-Massnahmen, die der Webseite schaden.

### Linkfarm
Als Linkfarm bezeichnet man eine Sammlung an Links auf einer Webseite, die für den User aber keinerlei Mehrwert haben. Es ist reines Linkspaming, mit dem der Webseitenbetreiber Geld verdienen will, da er Backlinks verkauft. Die Links passen in den meisten Fällen auch nicht zum Webseiteninhalt. Google straft dies ab, was schlussendlich zu negativen Folgen für die verlinkten Seiten führt.

### Duplicated Content
Von Duplicated Content spricht man, wenn Webseiteninhalte auf andere Seiten kopiert werden. Der Inhalt wird also dupliziert. Google bestraft dies, da es sich hier um geklauten Content halten könnte.

## Quellen 
Weiterbildung Digital-Marketing-Manager - Swiss Marketing Academy 

### Weiterführende Quellen:
* <a href="https://www.websiterating.com/de/research/google-search-engine-statistics/" target="_blank">websiterating.com - Statistik Google Suche</a>
* <a href="https://www.sistrix.de/news/klickwahrscheinlichkeiten-in-den-google-serps/" target="_blank">sistrix.de - Klickwahrscheinlichkeiten Google Suchergebnisse</a>
* <a href="https://developers.google.com/search/mobile-sites/mobile-first-indexing" target="_blank">developers.google.com - Mobile-First-Indexierung</a>