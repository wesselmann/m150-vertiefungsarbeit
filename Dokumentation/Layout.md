# Webseiten Layout
Ziel einer E-Commerce Webseite ist es letztendlich Produkte zu verkaufen. Um den Anwender oder die Anwenderin zu einem Kauf zu bringen, gibt es verschiedenste Methoden. Einer der wichtigsten Punkte ist hierbei die Layoutgestaltung der Webseite.

Der Aufbau eines Webshops kann den Weg des Users auf der Webseite leiten. Der optimale Weg eines Webseitenbesuchs besteht aus folgenden Etappen:


## 1. Webseite entdecken 
Die Nutzerin öffnet die Webseite und erhält innert Sekunden ihren ersten Eindruck.


## 2. Produktsuche oder Produktfindung 
Nicht alle Webnutzer wissen genau, nach was sie suchen. Manche browsen einfach nur und finden vielleicht etwas. Diese Anspruchsgruppe kann beispielsweise durch ein Banner mit einem Call-to-Action auf eine bestimmte Aktion oder ein Produkt aufmerksam gemacht werden. Solche Banner findet man meist direkt im oberen Bereich der. Ebenfalls können auf der Startseite weitere Produktvorschläge aufgelistet werden, um das Interesse der Kunden zu wecken.

_Beispiel Call-to-Action (digitec.ch)_

![Beispiel eines Call-to-Actions auf der Seite digitec.ch](/Dokumentation/images/C2A_Digitec.png "Beispiel Call-to-Action (digitec.ch)")

Auf der anderen Seite haben wir Personen, welche schon wissen, was sie suchen. Um diesen Leuten ihre Suche zu erleichtern, ist eine gute Suchfunktion essenziell. Mit ihr soll nicht nur nach einem einzelnen Produktnamen, sondern auch vielen weiteren Begriffen, wie Produktkategorien oder Produktnummern gesucht werden können. Somit wird eine Suche nach dem passenden Produkt um einiges einfacher für Kunden. Das Suchfeld befindet sich im Normalfall in der Mitte oder rechts im Header der Webseite.

Resultate sollen jeweils gefiltert werden können. Filterfunktionen finden sich meist am linken Seitenrand oder direkt über der Auflistung der Suchresultate.

_Beispiel Suchfeld (digitec.ch)_

![Beispiel eines Suchfeldes auf der Seite digitec.ch](/Dokumentation/images/Suchfeld_Digitec.png "Beispiel Suchfeld (digitec.ch)")


## 3. Produkt Seite 
Hat eine Besucherin nun ein Produkt gefunden, landet sie auf der Produktseite. Hier sind alle wichtigen Details über das Produkt zu finden. Es kann zum Einkaufswagen oder allenfalls zur Wunschliste hinzugefügt werden. Hier sind auch Bewertungen oder weitere Produktempfehlungen aufgelistet. Hierbei ist zu beachten, dass auch diese Seite kompakt gehalten wird, um Besuchende nicht mit Informationen zu überwältigen. Beispielsweise können Texte mit «mehr anzeigen» im Exempel unten ausgeklappt werden.

_Beispiel Produktseite (digitec.ch)_

![Beispiel einer Produktseite auf der Seite digitec.ch](/Dokumentation/images/Produktseite_Digitec.png "Beispiel Produktseite (digitec.ch)")


## 4. Einkaufswagen 
Hat der Kunde gefunden was er gesucht hat, kann er seine Käufe im Einkaufswagen überprüfen und darauffolgend den Kauf abschliessen. Um den Kauf zu erleichtern, kann meist von jeder Seite auf den Einkaufswagen zugegriffen werden.


## 5. Checkout
Nun muss der Kunde in unterschiedlichen Schritten seine Adressdaten eingeben, eine Zahlungsmethode wählen, usw. Dieser Ablauf soll klar sein und für Kunden und Kundinnen angenehm gehalten werden, indem beispielsweise per Default die Kundenadresse auch als die Lieferadresse verwendet wird (was der Kunde jederzeit anpassen kann). Ausserdem empfiehlt es sich, die einzelnen Checkout-Schritte auf mehrere Seiten aufzuteilen. Dies dient dazu, die Käufer strukturiert durch den Prozess hindurchzuführen und diese nicht mit Informationen zu erschlagen. Ist der Kauf abgeschlossen, ist es notwendig, dass dies auch für die Käufer und Käuferinnen klar ist. Dies kann mit einer Bestätigungsseite dargestellt werden.


## Responsive-Design
Bei der Kreation eines Webseitenlayouts sollte nicht nur der oben beschriebene Besuchsablauf des Users als Vorbild dienen, sondern auch das Responsive Design. 

Beim Responsive-Design geht es darum, dass die Webpage sich dem Bildschirmformat des Devices, welches die Webseite aufruft, anpasst. Somit kann eine grössenentsprechende Formatierung sichergestellt werden, ohne Verzerrungen oder ähnliche Darstellungsprobleme. Dafür wird normalerweise eine Breite festgelegt, ab welcher das Layout beispielsweise auf schmale Screens, wie von Smartphones, angepasst wird.

Das Responsive-Design ist nicht zu vernachlässigen, denn ein Webbesuch soll für jeden und jede so angenehm wie möglich sein. Das Angebot an anderen Webseiten und somit der Konkurrenz ist gross. Rund 7.8 Millionen Leute der Schweizer Bevölkerung haben ein Smartphone und etwa 75% aller Webzugriffe finden mittlerweile über mobile Geräte statt. 


## Quellen
* <a href="https://www.smashingmagazine.com/2011/01/guidelines-for-responsive-web-design/" target="_blank">smashingmagazine.com - Responsive Webdesign</a>
* <a href="https://www.toptal.com/designers/e-commerce/ecommerce-ux-design-principles" target="_blank">toptal.com - UX Design Prinzipien</a>
* <a href="https://www.toptal.com/designers/e-commerce/ultimate-ecommerce-design-guide" target="_blank">toptal.com - eCommerce Design Guide</a>
* <a href="https://www.youtube.com/watch?v=TDRhwSfxYkg" target="_blank">YouTube - Layout</a>